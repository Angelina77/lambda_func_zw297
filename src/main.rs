use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use lambda_http::{run, service_fn, Body, Error, Request, Response};
use aws_sdk_s3::Client as S3Client;
use serde::{Deserialize, Serialize};
use aws_config::Region;

#[derive(Debug, Serialize, Deserialize)]
struct MyData {
    list: String,
}

#[derive(Serialize, Deserialize, Default)]
struct ResData {
    method: String,
    results: Vec<SubResData>,
}

#[derive(Serialize, Deserialize, Default)]
struct SubResData {
    status_message: String,
    squared_list: Vec<i32>, 
}

async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let config = aws_config::from_env().region(Region::new("us-east-2")).load().await;
    let client = S3Client::new(&config);
    let bucket = "squarelist"; 
    let key = "mydata.csv"; 
    let resp = client.get_object().bucket(bucket).key(key).send().await?;
    let body = resp.body.collect().await?.into_bytes();
    let csv_content = String::from_utf8(body.to_vec()).expect("Found invalid UTF-8");
    let mut rdr = csv::Reader::from_reader(csv_content.as_bytes());
    let mut res_data: ResData = Default::default();
    res_data.method = event.method().as_str().to_string();
    for result in rdr.deserialize() {
        let mut sub_res_data: SubResData = Default::default();
        let my_data: MyData = result?;
        let list = my_data.list;
        let trimmed = &list[1..list.len()-1];
        let vec: Vec<i32> = trimmed.split(',')
                               .map(|s| s.trim().parse::<i32>().map(|num| num * num))
                               .collect::<Result<Vec<i32>, _>>()
                               .unwrap_or(Vec::new());
        sub_res_data.squared_list = vec;
        sub_res_data.status_message = "Success".to_string();
        res_data.results.push(sub_res_data);
    }
    let resp = Response::builder()
        .status(200)
        .header("content-type", "application/json")
        .body(serde_json::to_string(&res_data).unwrap().into())
        .map_err(Box::new)?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
