# AWS Lambda Function

## Description
This AWS Lambda function is designed to process lists and return lists with every element squared.
[Click to view response of the function](https://0jwjzifwub.execute-api.us-east-2.amazonaws.com/default/square_list)




## Prerequisites
Before deploying and running this Lambda function, do the following steps:

- AWS account
   - Create an AWS account
   - Go to IAM service, create access key and save the access key ID and secret access key
- AWS CLI installed and configured
   - Download and install the AWS CLI from the AWS CLI website.
   - Once installed, configure the CLI by running this in terminal. Enter your IAM access key ID and secret access key when prompted.
   ```bash
   aws configure
   ```
   
- install Rust programming language
- install cargo lambda
   ```bash
   cargo install cargo-lambda
   ```


## Prepare data
Go to s3 service to create a new s3 bucket by uploading a csv.
![bucket](image.png)

The csv I provide here is basically two lists to be processed.

<img src="image-1.png" alt="alt text" width="300"/>

## Implementing the function

### Data Structures

- MyData: A struct for deserializing data from CSV format. It has a single field list which contains a String.
- ResData: A struct for serializing the response data into JSON. It contains the request method and a vector of SubResData.
- SubResData: A struct for holding the status message and a list of squared integers.


### The function_handler Function

- This is the core of the Lambda function, where the processing of the incoming HTTP event occurs.
It loads the AWS configuration and creates an S3 client.
It specifies the S3 bucket and object key to retrieve the CSV file.
- It reads the object content as a string and uses a CSV reader to deserialize each line into the MyData struct.
For each MyData entry, it parses the list string to extract integers, squares them, and adds the result to the ResData struct.
- Finally, it constructs an HTTP Response with a 200 status code and a body containing the JSON-serialized ResData.

### The main Function

- It sets up the logging configuration using tracing_subscriber.
- It initializes the Lambda function to run, specifying function_handler as the handler for incoming events.

## Environment
edit Crago.toml to cover necessary imports and dependencies

## build and deploy
run this command to build a new project.
   ```bash
   cargo lambda new my_rust_lambda
   ```
cd the new directory and edit src/main.rs and Cargo.toml and run commands:
   ```bash
   cargo lambda build
   ```
   ```bash
   cargo lambda deploy
   ```
Go to the aws website and go to Lambda service to find the function square_list:

Add a API Gateway trigger(choose REST):
![alt text](image-2.png)

Go to permissions and you will find that a role is created. Click on that role.![alt text](image-3.png)
Add permissions:AdministratorAccess

Now we can click on [API endpoint](https://0jwjzifwub.execute-api.us-east-2.amazonaws.com/default/square_list) to view the response of the function:

![alt text](image-4.png)
We can see that it gives back the squared lists.
[1,3,5,7]->[1,9,25,49];
[2,4,6,8]->[4,16,36,64]
